/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public class Rabbit extends Animal {
    public Rabbit(String name, String color) {
        super(name, color, 4);
        System.out.println("Rabbit created");
    }
    public void jump(){
        System.out.println("Duck: " + name + " jump!!!");
    }
    @Override
    public void walk() {
        System.out.println("Rabbit: " + name + "walk with " + numberOfLegs + " legs: ");
    }
    
    
    @Override
    public void speak() {
        System.out.println("Rabbit: " + name + "speak > Purr!!!" );
    }

}
