/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Aiai", "White",0);
        animal.speak();
        animal.walk();
        
        Dog momo = new Dog("Momo" ,"Brown");
        momo.speak();
        momo.walk();
        
        Cat bena = new Cat("Bena","Black");
        bena.speak();
        bena.walk();
        
        Rabbit bunny = new Rabbit("Bunny","OrangeandWhite");
        bunny.speak();
        bunny.walk();
        bunny.jump();
    }
}
